package airport

type conn interface {
	Read([]byte) (int, error)
	Write([]byte) (int, error)
}

type Observable interface {
	addObserver(observer)
	remove(observer)
	notify()
}

type Observer interface {
	update()
}

type observer struct {
	conn conn
}

func NewObserver(conn conn) *observer {
	return &observer{conn}
}

func (o observer) update() {

}
