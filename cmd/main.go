package main

import (
	"aiport_info_sys/client"
	"aiport_info_sys/server"
	"os"
)

func main() {
	if len(os.Args) > 1 {
		if os.Args[1] == "server" {
			server.Start()
		}
	} else {
		client.Start()
	}
}
