package airport

type info struct {
	flightNr     int
	gate         *gate
	plane        *plane
	boardingtime *timeslot
}

func newInfo() *info {
	info := info{}
	info.flightNr = 1
	return &info
}

func (i *info) findGate(gates []*gate) *gate {
	for _, gate := range gates {
		for _, timeslot := range gate.timeslots {
			if timeslot.free {
				timeslot.free = false
				i.gate = gate
				i.boardingtime = timeslot
				return gate
			}
		}
	}
	return nil
}

func (i *info) findPlane(planes []*plane) *plane {
	for _, plane := range planes {
		if plane.free {
			plane.free = false
			i.plane = plane
			return plane
		}
	}
	return nil
}
