package server

import (
	airport "aiport_info_sys"
	"fmt"
	"net"
)

// only needed below for sample processing

func Start() {
	fmt.Println("Launching server...")
	// start system
	system := airport.NewSystem()
	//makes so it listens to commands and the tcp use threads
	go serverShell(system)
	// listen on all interfaces
	ln, _ := net.Listen("tcp", ":8081")

	connChannel := make(chan conn)
	go func() {
		for {
			// accept connection on port
			conn, _ := ln.Accept()
			connChannel <- conn
		}
	}()

	messageChannel := make(chan string)
	connChannelformessages := make(chan conn)
	//spawn readers
	go func() {
		for {
			connection := <-connChannel
			//read to channel from connection
			go readtofrom(messageChannel, connChannelformessages, connection)
		}
	}()

	for {
		message := <-messageChannel
		conn := <-connChannelformessages

		newmessageBytes := system.CheckFlightsForPassenger(message)
		if newmessageBytes != nil {
			system.AddObserver(airport.NewObserver(conn))
		} else {
			newmessageBytes = []byte("No matching Flight")
		}
		byten := []byte("\n")
		newmessageBytes = append(newmessageBytes, byten[0])

		// output message received
		fmt.Print("\nMessage Received:", string(message), "\n->")
		// sample process for string received
		//newmessage := strings.ToUpper(message)
		// send new string back to client
		conn.Write(newmessageBytes)
	}
	//output message from channel and sent it back as upper
}
