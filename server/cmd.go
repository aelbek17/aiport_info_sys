package server

import (
	airport "aiport_info_sys"
	"fmt"
	"strconv"
	"strings"
)

func help(commands map[string]interface{}) {
	fmt.Println("commands:")
	for k := range commands {
		fmt.Println("- " + k)
	}
	fmt.Println("To show this list agian type 'help'")
}

func commands(s airport.System, command string) {
	commandparameters := strings.Fields(command)
	//no need for a map as is could just call functions directly..
	//map can be moved somwhere else, if the functions in map all took the same input, switch could be omitted
	m := map[string]interface{}{
		//add commands here
		"registerPlane": airport.System.RegisterPlanes,
		"makeGate":      airport.System.MakeGates,
		"help":          help, //prints out the commands
		"addpassenger":  airport.System.Addpassenger,
		"printFlights":  airport.System.PrintFlights,
	}
	switch commandparameters[0] { //cases needs error handling if parameter is missing
	case "help":
		m["help"].(func(map[string]interface{}))(m)
	case "makeGate":
		fmt.Println("building a new gate")
		parameter, err := strconv.Atoi(commandparameters[1])
		if err == nil {
			m["makeGate"].(func(airport.System, int))(s, parameter)
		}
	case "registerPlane":
		fmt.Println("Registering plane(s)")
		parameter, err := strconv.Atoi(commandparameters[1])
		if err == nil {
			m["registerPlane"].(func(airport.System, int))(s, parameter)
		}
	case "addpassenger":
		passerngerID := ""
		if len(commandparameters) > 1 {
			fmt.Println("adding passenger to flight " + commandparameters[1])
			passerngerID = m["addpassenger"].(func(airport.System, string) string)(s, commandparameters[1])
		} else {
			passerngerID = m["addpassenger"].(func(airport.System, string) string)(s, "")
		}
		fmt.Println("passenger ID: " + passerngerID)
	case "printFlights":
		m["printFlights"].(func(airport.System))(s)
	default:
		fmt.Println("unrecognized command, type help for commands")
	} //and here
}
