package server

import (
	airport "aiport_info_sys"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func serverShell(s airport.System) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Server Shell")
	fmt.Println("---------------------")

	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\r\n", "", -1)
		commands(s, text)
	}
}

type conn interface {
	Read([]byte) (int, error)
	Write([]byte) (int, error)
}

func readtofrom(messageChannel chan string, connChannel chan conn, conn conn) {
	//<-cts
	for {
		message, _ := bufio.NewReader(conn).ReadString('\n')
		message = strings.Replace(message, "\r\n", "", -1)
		message = strings.Replace(message, "\n", "", -1)
		messageChannel <- message
		connChannel <- conn
	}
}
