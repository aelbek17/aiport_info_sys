package airport

import (
	"fmt"

	"github.com/rs/xid"
)

type flight struct {
	info         *info
	passengerIDs []string
	id           string
}

func newFlight(gates []*gate, planes []*plane) *flight {
	id := xid.New().String()
	flight := flight{id: id}
	flight.info = newInfo()
	flight.info.findGate(gates) //also sets boardingtime in info
	flight.info.findPlane(planes)
	return &flight
}

func (f *flight) addPassenger(passengerID string) {
	f.passengerIDs = append(f.passengerIDs, passengerID)
}

func (f flight) checkPassengerID(passengerID string) bool {
	for _, ID := range f.passengerIDs {
		if ID == passengerID {
			return true
		}
	}
	return false
}

func (f flight) printID() {
	fmt.Println(f.id)
}
