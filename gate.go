package airport

import "github.com/rs/xid"

type gate struct {
	id        string
	timeslots []*timeslot
}

func newGate() *gate {
	id := xid.New().String()
	gate := gate{id, nil}
	gate.makeTimeslots()
	return &gate
}

func (g *gate) makeTimeslots() {
	for i := 0; i < 24; i++ {
		g.timeslots = append(g.timeslots, &timeslot{i * 60, (i + 1) * 60, true})
	}
}
