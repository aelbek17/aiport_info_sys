package airport

type flightNrContainer struct {
	lastNr    int
	highestNr int
}

func newflightNrContainer() *flightNrContainer {
	return &flightNrContainer{0, 1000}
}

func (f flightNrContainer) newflightNr() int {
	if f.lastNr == f.highestNr {
		f.lastNr = 0
	}
	f.lastNr++
	return f.lastNr
}
