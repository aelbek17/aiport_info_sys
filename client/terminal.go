package client

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func Start() {
	// connect to this socket
	conn, _ := net.Dial("tcp", "127.0.0.1:8081")

	fmt.Println("Type in passenger ID:")
	for {
		// read in input from stdin
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("->")
		text, _ := reader.ReadString('\n')
		// send to socket
		fmt.Fprintf(conn, text+"\n")
		// listen for reply
		message, _ := bufio.NewReader(conn).ReadString('\n')
		//fmt.Print("Message from server: " + message)
		displayflightinfo(message)
	}
}
