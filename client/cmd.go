package client

import (
	airport "aiport_info_sys"
	"fmt"
)

func help(commands map[string]interface{}) {
	fmt.Println("commands:")
	for k := range commands {
		fmt.Println("- " + k)
	}
	fmt.Println("To show this list agian type 'help'")
}

func commands(s airport.System, command string) {
	//no need for a map as is could just call functions directly..
	//map can be moved somwhere else, if the functions in map all took the same input, switch could be omitted
	m := map[string]interface{}{
		//add commands here
		"registerPlane": airport.System.RegisterPlanes,
		"makeGate":      airport.System.MakeGates,
		"help":          help, //prints out the commands
	}
	switch command {
	case "help":
		m["help"].(func(map[string]interface{}))(m)
	case "makeGate":
		fmt.Println("building a new gate")
		m["makeGate"].(func(airport.System, int))(s, 1)
	case "registerPlane":
		fmt.Println("Registering plane")
		m["registerPlane"].(func(airport.System, int))(s, 1)
	default:
		fmt.Println("unrecognized command, type help for commands")
	} //and here
}
