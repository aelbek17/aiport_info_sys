package client

import (
	airport "aiport_info_sys"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func clientShell(s airport.System) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Client terminal")
	fmt.Println("---------------------")

	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\r\n", "", -1)
		commands(s, text)
	}
}
