package client

import (
	"fmt"
	"strconv"
	"strings"
)

func displayflightinfo(message string) {
	fields := strings.Fields(message)
	if len(fields) != 5 {
		fmt.Println(fields)
		return
	}
	fmt.Println(
		" gateID: "+fields[0], "\n",
		"planeID: "+fields[1], "\n",
		"flight nr.: "+fields[2], "\n",
		"boardingtime: "+convertMinToTime(fields[3])+" - "+convertMinToTime(fields[4]), "\n",
	)
}

func convertMinToTime(min string) string {
	minint, err := strconv.Atoi(min)
	if err != nil {
		panic(err)
	}
	hours := strconv.Itoa(minint / 60)
	if len(hours) < 2 {
		hours = "0" + hours
	}
	minutes := strconv.Itoa(minint % 60)
	if len(minutes) < 2 {
		minutes = "0" + minutes
	}
	time := string(hours) + ":" + minutes
	return time
}
