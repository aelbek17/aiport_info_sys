# Lidt om fly

## Slots
I flybrancen har fly "slots" det bestemmer hvornaar et fly skal lande og lætte.
Hvis et fly bliver forsinket lætter det ikke nødvendighvis ASAP men først ved næste ledige slot.
Altsaa naar det lander hvis til tiden kan vi forvente de letter til tiden.
Hvis det lander for sent kan vi mulighvis lave en "find next slot that is more than 45 minutes away".
Paa den maade kan vi bestemme hvornaar flyet lætter igen.

## Gates
Right, i større lufthavne er der flere selvskaber der ver har sine gates (sas eller menzies i kbh).
Men da det er en lille lufthavn er der normalt kun 1.
Aalborg lufthavn har 7 gates, kunne være vi skulle simulere den?
Korte grund til skift: Forsinkelse.
Hvis du er forsinket kan lufthavnen godt skifte din gate.
Kan ogsaa være for maaange andre aarsager, saa man kan bare smide en procent chance for gate switch?

## Skærme
Der er 2 slags skærme i en lufthavn, infoskærm og den lokale skærm ved gate.

### Info screen
Findes overalt i lufthavne og skal faa folk hen til deres gate i god tid.

Indeholder:
Takeoff time
Company
Destination
Flight Nr.
Codeshare Nr (nok ikke relevandt)
gate nr
Remarks som: Boarding now, closing og goto gate.

### Boarding screen
Er ved vær eneste gate og Indeholder:
Takeoff time
Company
Destination
Flight Nr.
Codeshare Nr. (igen nok ikke relevandt)
Gate nr.
Remarks som: Boarding now, closing og goto gate.

## Foreskelen
Info screen skal vise alle fly, mens Boarding screen kun skal hvis det fly der er ved den gate.
