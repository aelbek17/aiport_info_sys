package airport

import "github.com/rs/xid"

type plane struct {
	id      string
	airline string
	free    bool
}

func newPlane() *plane {
	id := xid.New().String()
	return &plane{id, "airways", true}
}
