package airport

import (
	"fmt"
	"strconv"

	"github.com/rs/xid"
)

type System interface {
	MakeGates(numgerOfGates int)
	RegisterPlanes(numgerOfPlanes int)
	CheckFlightsForPassenger(passengerID string) []byte
	Addpassenger(flightID string) string
	PrintFlights()
	AddObserver(observer Observer)
}

type system struct {
	Observers []Observer
	Gates     []*gate
	flights   []*flight
	planes    []*plane // gets added (and removed?) as they are used
	flightNrs *flightNrContainer
}

func NewSystem() *system {
	system := system{}
	system.MakeGates(10)
	system.RegisterPlanes(10)
	system.reserveFlight()
	return &system
}

func (s *system) MakeGates(numgerOfGates int) {
	for i := 0; i < numgerOfGates; i++ {
		s.Gates = append(s.Gates, newGate())
	}
}

func (s *system) RegisterPlanes(numgerOfPlanes int) {
	for i := 0; i < numgerOfPlanes; i++ {
		s.planes = append(s.planes, newPlane())
	}
}

func (s *system) reserveFlight() {
	s.planes = append(s.planes, newPlane())
	s.flights = append(s.flights, newFlight(s.Gates, s.planes))
}

func (s *system) Addpassenger(flightID string) string {
	id := xid.New().String()
	if flightID == "" {
		flightID = s.findFlight()
	}
	for _, flight := range s.flights {
		if flight.id == flightID {
			flight.addPassenger(id)
			return id
		}
	}
	return "no flights available"
}

func (s *system) findFlight() string {
	if len(s.flights) != 0 {
		return s.flights[0].id
	}
	return ""
}

func (s *system) CheckFlightsForPassenger(passengerID string) []byte {
	for _, flight := range s.flights {
		if flight.checkPassengerID(passengerID) {
			return s.packinfo(flight.info)
		}
	}
	return nil
}

func (s *system) packinfo(info *info) []byte {
	infostring := info.gate.id + " " + info.plane.id + " " + strconv.Itoa(info.flightNr) + " " + strconv.Itoa(info.boardingtime.start) + " " + strconv.Itoa(info.boardingtime.end)
	return []byte(infostring)
}

func (s system) PrintFlights() {
	if len(s.flights) == 0 {
		fmt.Println("no flights registered")
	}
	for _, flight := range s.flights {
		flight.printID()
	}
}

//observable interface methods
func (s system) AddObserver(observer Observer) {
	s.Observers = append(s.Observers, observer)
}
func (s system) remove(observer) {

}

func (s system) notify() {
	for _, o := range s.Observers {
		o.update()
	}
}
