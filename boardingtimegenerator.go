package airport

import "math/rand"

type boardingtime struct {
	timeinterval string
}

func newBoardingtime() *boardingtime {
	time := string(rand.Intn(23))
	return &boardingtime{time}
}
